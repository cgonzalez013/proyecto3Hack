class VotesController < ApplicationController

	before_action :authenticate, except:[:index]

	rescue_from ActiveRecord::RecordNotFound,with: :r_not_found

	def index
		votes= Vote.all

		render json: votes

	end

	def show
		vote= Vote.find(params[:id])

		render json: vote

	end

	def create
		vote = Vote.new(permit_params)
		@user = User.where(login_params).take
		@user_id = @user.id
		vote.user_id= @user_id
		vote.set_rating

		if vote.save
			render json: {message: "Vote succesfully created"}
		else
			render json: {message: "Vote not succesfully created"}
		end

	end

	def update
		vote= Vote.find(params[:id])
		vote.set_rating

		if vote.update(permit_params)
			render json: {message: "Vote succesfully updated"}
		else
			render json: {message: "Vote not succesfully updated"}
		end
	end

	def destroy
		vote= Vote.find(params[:id])

		if vote.destroy
			render json: {message: "Vote succesfully destroyed"}
		else
			render json: {message: "Vote not succesfully destroyed"}
		end

	end

	protected

	def login_params
		params.require(:user).permit(:log_name, :password)
		
	end

	def permit_params
		params.require(:vote).permit(:value, :meme_id)

	end

	def r_not_found
		render json: {message: "Error 404 register not found"}, status: 404
	end

end