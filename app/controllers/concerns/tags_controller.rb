class TagsController < ApplicationController

	before_action :authenticate, except:[:show_public_tag]
	rescue_from ActiveRecord::RecordNotFound,with: :r_not_found

	def index
		tags= Tag.all

		render json: tags.as_json

	end

	def show
		tag= Tag.find(params[:id])

		render json: tag

	end

	def create
		tag = Tag.new(permit_params)
		meme_tag = MemeTag.new(relation_params)
		meme_tag.tag_id = tag.id

		if tag.save
				render json: {message: "Tag succesfully created"}
		else
			render json: {message: "Tag not succesfully created"}
		end
	end

	def update
		tag= Tag.find(params[:id])

		if tag.update(permit_params)
			render json: {message: "Tag succesfully updated"}
		else
			render json: {message: "Tag not succesfully updated"}
		end
	end

	def destroy
		tag= Tag.find(params[:id])

		if tag.destroy
			render json: {message: "Tag succesfully destroyed"}
		else
			render json: {message: "Tag not succesfully destroyed"}
		end

	end

	private

	def relation_params
		params.require(:meme_tag).permit(:meme_id)

	end

	def permit_params
		params.require(:tag).permit(:private, :src_image, :top_text, :bottom_text, :src_url, :category_id)

	end

	def r_not_found
		render json: {message: "Error 404 register not found"}, status: 404
	end

end