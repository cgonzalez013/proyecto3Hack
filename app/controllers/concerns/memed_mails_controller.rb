class MemedMailsController < ApplicationController

	before_action :authenticate
	rescue_from ActiveRecord::RecordNotFound,with: :r_not_found

	def index
		memed_maild_emails= MemedMail.all

		render json: memed__mails.as_json

	end

	def show
		memed_mail= MemedMail.find(params[:id])

		render json: memed_mail

	end

	def create
		@meme = Meme.where(meme_params).take
		@meme_id = @meme.id
		memed_mail = MemedMail.new(permit_params)
		memed_mail.meme_id= @meme_id

		if memed_mail.save
			render json: {message: "The MemedMail was succesfully created"}
			MemeMailer.send_image('#{@meme_id}sent_from_Proyecto3Hack', "app/assets/images/user_#{memed_mail.meme_id}meme", memed_mail.email).deliver_now
		else
			render json: {message: "MemedMail not succesfully created"}
		end
	end

	def update
		memed_mail= MemedMail.find(params[:id])

		if memed_mail.update(permit_params)
			render json: {message: "MemedMail succesfully updated"}
		else
			render json: {message: "MemedMail not succesfully updated"}
		end
	end

	def destroy
		memed_mail= MemedMail.find(params[:id])

		if memed_mail.destroy
			render json: {message: "MemedMail succesfully destroyed"}
		else
			render json: {message: "MemedMail not succesfully destroyed"}
		end

	end

	private

	def meme_params
		params.require(:meme).permit(:id)
		
	end

	def permit_params
		params.require(:memed_mail).permit(:email)

	end

	def r_not_found
		render json: {message: "Error 404 register not found"}, status: 404
	end

end