class MemesController < ApplicationController

	before_action :authenticate, except:[:show_public_meme]
	rescue_from ActiveRecord::RecordNotFound,with: :r_not_found

	def index
		memes= Meme.all

		render json: memes.as_json

	end

	def show_listed_memes
		meme= Meme.new
		list= meme.get_src_meme_list

		render json: list
		
	end

	def show
		meme= Meme.find(params[:id])

		render json: meme

	end

	def create
		meme = Meme.new(permit_params)
		@user = User.where(login_params).take
		@user_id = User.where(login_params).take.id
		meme.user_id= @user_id

		if meme.save
			if meme.user_id= nil
				render json: {message: "The Meme was created, but no one owns it so it wasn't saved :C"}
			else
				render json: {message: "Meme number #{meme.id} succesfully created. #{@user.log_name} owns it; his id is #{@user.id}"}
				meme.save_meme
			end
		else
			render json: {message: "Meme not succesfully created"}
		end
	end

	def update
		meme= Meme.find(params[:id])

		if meme.update(permit_params)
			render json: {message: "Meme succesfully updated"}
		else
			render json: {message: "Meme not succesfully updated"}
		end
	end

	def destroy
		meme= Meme.find(params[:id])

		if meme.destroy
			render json: {message: "Meme succesfully destroyed"}
		else
			render json: {message: "Meme not succesfully destroyed"}
		end

	end

	def show_public_memes
		@meme= Meme.where(private: "0")
		@meme= @meme.sort

		render json: @meme
		
	end

	private

	def login_params
		params.require(:user).permit(:log_name, :password)
		
	end

	def permit_params
		params.require(:meme).permit(:private, :src_image, :top_text, :bottom_text, :src_url, :category_id)

	end

	def r_not_found
		render json: {message: "Error 404 register not found"}, status: 404
	end

end