class CategoriesController < ApplicationController

	rescue_from ActiveRecord::RecordNotFound,with: :r_not_found

	def index
		categories= Category.all

		render json: categories.as_json(include: :memes)

	end

	def show
		category= Category.find(params[:id])

		render json: category
	end

	def create
		category = Category.new(permit_params)

		if category.save
			render json: {message: "Register succesfully created"}
		else
			render json: {message: "Register not succesfully created"}
		end

	end

	def update
		category= Category.find(params[:id])

		if category.update(permit_params)
			render json: {message: "Register succesfully updated"}
		else
			render json: {message: "Register not succesfully updated"}
		end
	end

	def destroy
		category= Category.find(params[:id])

		if category.destroy
			render json: {message: "Register succesfully destroyed"}
		else
			render json: {message: "Register not succesfully destroyed"}
		end

	end

	private

	def permit_params
		params.require(:category).permit(:name, :memes_attributes)

	end

	def r_not_found
		render json: {message: "Error 404 register not found"}, status: 404
	end

end