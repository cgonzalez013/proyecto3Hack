#require 'rubysl-securerandom'

class UsersController < ApplicationController

	before_action :authenticate, except:[:create, :login, :index]

	rescue_from ActiveRecord::RecordNotFound,with: :r_not_found

	def login
		@user = User.where(login_params).take
		if @user
			@user.password= SecureRandom.uuid.gsub(/\-/,'')
			@user.save
			render json: @user
		else
			render json: {message: "user not found"}
		end
		return @user
	end

	def log_out
		@user.password= nil

		if @user.password==nil
			render json: {message: "User logged out"}
			@user.save
		else
			render json: {message: "User not logged out"}
		end
		
	end

	def index

		render json: @user

	end

	def show
		user= User.find(params[:id])

		render json: user

	end

	def create
		user = User.new(permit_params)

		if user.save
			render json: {message: "User succesfully created"}
		else
			render json: {message: "User not succesfully created"}
		end

	end

	def update
		user= User.find(params[:id])

		if user.update(permit_params)
			render json: {message: "User succesfully updated"}
		else
			render json: {message: "User not succesfully updated"}
		end
	end

	def destroy
		user= User.find(params[:id])

		if user.destroy
			render json: {message: "User succesfully destroyed"}
		else
			render json: {message: "User not succesfully destroyed"}
		end

	end

	protected

	def login_params
		params.require(:user).permit(:log_name, :password)
		
	end

	def permit_params
		params.require(:user).permit(:log_name, :email)

	end

	def r_not_found
		render json: {message: "Error 404 register not found"}, status: 404
	end

end