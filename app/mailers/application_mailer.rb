class ApplicationMailer < ActionMailer::Base
  default from: "carlos"
  layout 'mailer'
end
