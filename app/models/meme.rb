require 'rest-client'

class Meme < ActiveRecord::Base
  belongs_to :user
  belongs_to :category

  has_many :meme_tags
	has_many :tags, through: :meme_tags
	has_many :memed_mails
	has_many :votes

	before_create :post_to_memecaptain_api
	#after_save :save_meme

	def get_src_meme_list
		@meme_list_json= JSON.parse(RestClient.get('http://memecaptain.com/src_images', accept: :json))
		
	end

	def set_array_with_meme_list_json
		meme_list_array= []
		@meme_list_json.each do |image|

			meme_list_item= 
			{
				"image_id": image["id_hash"],
				"content_type": image["content_type"],
				"name": image["name"],
				"image_url": image["image_url"]
				
			}
			meme_list_array.push(meme_list_item)
		end
		return meme_list_array
		
	end

	def set_request
		request=
		{
		  "private": self.private,
		  "src_image_id": self.src_image,
		  "captions_attributes": [
		    {
		      "text": self.top_text,
		      "top_left_x_pct": 0.05,
		      "top_left_y_pct": 0.0,
		      "width_pct": 0.9,
		      "height_pct": 0.25
		    },
		    {
		      "text": self.bottom_text,
		      "top_left_x_pct": 0.05,
		      "top_left_y_pct": 0.75,
		      "width_pct": 0.9,
		      "height_pct": 0.25
		    }
		  ]
		}
		return request
	end

	def set_wait_time

#This doesn't work because the list of src_images is incomplete, plus it takes way too long to request and I got bored. 
#you get to wait a minute for the image, deal with it

#		set_array_with_meme_list_json.each do |image|
#				if self.src_image== image["image_id"] || self.src_image== '0lcObA'
#					if image["content_type"]== "image/gif" ||  self.src_image== '0lcObA'
						sleep(60)
#					else
#						sleep(10)
#					end
#				end
#		end
		
	end

	def save_meme
		if self.src_url!= nil
			set_wait_time
			response= RestClient.get(self.src_url)
			File.open("app/assets/images/user_#{self.id}meme", "wb"){|file| file.write(response)}
		end
	end

	def post_to_memecaptain_api
		require 'rest-client'
		get_src_meme_list
		request= set_request
		status_url_json= JSON.parse(RestClient.post('http://memecaptain.com/gend_images', request.to_json, content_type: :json, accept: :json))
		self.src_url= status_url_json['status_url']	
	end

end
