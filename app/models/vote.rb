class Vote < ActiveRecord::Base
belongs_to :meme
belongs_to :user

	def set_rating
		meme= Meme.find(self.meme_id)
		if meme!= nil
			total= self.value
			votes_from_same_meme= Vote.where(user_id: self.meme_id)
			previous_votes= votes_from_same_meme.each do |vote|
				total= total+ vote.value
			end
			meme.rating= total / (votes_from_same_meme.size)
			meme.update
			
		end

	end

end
