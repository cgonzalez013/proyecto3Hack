class Category < ActiveRecord::Base
	has_many :memes

	accepts_nested_attributes_for :memes
end
