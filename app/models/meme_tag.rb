class MemeTag < ActiveRecord::Base
  belongs_to :meme
  belongs_to :tag

  validates_presence_of :meme_id, :tag_id

end
