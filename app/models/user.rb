class User < ActiveRecord::Base
	has_many :memes
	validates_uniqueness_of :log_name, :email
end
