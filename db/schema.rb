# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160206025332) do

  create_table "categories", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "meme_tags", force: :cascade do |t|
    t.integer  "meme_id",    limit: 4
    t.integer  "tag_id",     limit: 4
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  add_index "meme_tags", ["meme_id"], name: "index_meme_tags_on_meme_id", using: :btree
  add_index "meme_tags", ["tag_id"], name: "index_meme_tags_on_tag_id", using: :btree

  create_table "memed_mails", force: :cascade do |t|
    t.string   "email",      limit: 255
    t.integer  "meme_id",    limit: 4
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_index "memed_mails", ["meme_id"], name: "index_memed_mails_on_meme_id", using: :btree

  create_table "memes", force: :cascade do |t|
    t.string   "private",     limit: 255
    t.string   "src_image",   limit: 255
    t.string   "top_text",    limit: 255
    t.string   "bottom_text", limit: 255
    t.string   "src_url",     limit: 255
    t.integer  "rating",      limit: 4
    t.integer  "user_id",     limit: 4
    t.integer  "category_id", limit: 4
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  add_index "memes", ["category_id"], name: "index_memes_on_category_id", using: :btree
  add_index "memes", ["user_id"], name: "index_memes_on_user_id", using: :btree

  create_table "tags", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "log_name",   limit: 255
    t.string   "email",      limit: 255
    t.string   "password",   limit: 255
    t.string   "token",      limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "votes", force: :cascade do |t|
    t.integer  "value",      limit: 4
    t.integer  "meme_id",    limit: 4
    t.integer  "user_id",    limit: 4
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  add_index "votes", ["meme_id"], name: "index_votes_on_meme_id", using: :btree
  add_index "votes", ["user_id"], name: "index_votes_on_user_id", using: :btree

  add_foreign_key "meme_tags", "memes"
  add_foreign_key "meme_tags", "tags"
  add_foreign_key "memed_mails", "memes"
  add_foreign_key "memes", "categories"
  add_foreign_key "memes", "users"
  add_foreign_key "votes", "memes"
  add_foreign_key "votes", "users"
end
