class CreateMemes < ActiveRecord::Migration
  def change
    create_table :memes do |t|
      t.string :private
      t.string :src_image
      t.string :top_text
      t.string :bottom_text
      t.string :src_url
      t.integer :rating
      t.references :user, index: true, foreign_key: true
      t.references :category, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
