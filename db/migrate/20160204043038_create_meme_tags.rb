class CreateMemeTags < ActiveRecord::Migration
  def change
    create_table :meme_tags do |t|
      t.references :meme, index: true, foreign_key: true
      t.references :tag, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
