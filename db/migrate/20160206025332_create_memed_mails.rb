class CreateMemedMails < ActiveRecord::Migration
  def change
    create_table :memed_mails do |t|
      t.string :email
      t.references :meme, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
