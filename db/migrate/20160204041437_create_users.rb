class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :log_name
      t.string :email
      t.string :password
      t.string :token

      t.timestamps null: false
    end
  end
end
