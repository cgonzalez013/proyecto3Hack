# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Category.create([{ name: 'Funny'}])
Category.create([{ name: 'Not appropiate'}])
Category.create([{ name: 'Serious'}])

User.create([{log_name: 'MawileApocalypse', email: 'carloseduardogonzalezmendoza@gmail.com', password: 'carlosisdabes', token: 'A1B2C3D4E5'}])

Meme.create([{private: false, src_image: '0lcObA', top_text: 'Este es Carlos', bottom_text: 'En Hack'}])

Tag.create([{name: 'wow'}, {name: 'WoWsErSbOwSeRs!!1'}])